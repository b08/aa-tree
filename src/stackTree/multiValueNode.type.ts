import { AANode } from "../aaTree/aaNode.type";
import { createAANode } from "../aaTree/createAANode";

export interface MultiValueNode<T, TKey> extends AANode<TKey> {
  items: T[];
  left: MultiValueNode<T, TKey>;
  right: MultiValueNode<T, TKey>;
}

export function createMultiValueNode<T, TKey>(item: T, key: TKey): MultiValueNode<T, TKey> {
  return {
    ...<MultiValueNode<T, TKey>>createAANode(key),
    items: [item],
  };
}
