export interface AANode<TKey> {
  key: TKey;
  level: number;
  left: AANode<TKey>;
  right: AANode<TKey>;
}
