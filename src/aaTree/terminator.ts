import { AANode } from "./aaNode.type";

export const terminator: any = {
  key: null,
  level: 0,
};

terminator.right = terminator;

export function isTerminator<TKey>(node: AANode<TKey>): boolean {
  return node === terminator;
}
