import { AANode } from "../../../src/aaTree/aaNode.type";
import { isLeaf } from "../../../src/aaTree/isLeaf";
import { isTerminator } from "../../../src/aaTree/terminator";
import { IExpect } from "@b08/test-runner";

// 1.) The level of leaf node is 1.
// 2.) The level of left child is exactly one less than of its parent.
// 3.) The level of every right child is is equal to or one less than of its parent.
// 4.) The level of every right grandchild is strictly less than that of its grandparent.
// 5.) Every node of level greater than one has two children.

export function validateAaTreeConstraints<TKey>(expect: IExpect, node: AANode<TKey>): void {
  if (isLeaf(node)) {
    expect.equal(node.level, 1);
    return;
  }

  if (!isTerminator(node.left)) {
    expect.equal(node.left.level, node.level - 1);
    validateAaTreeConstraints(expect, node.left);
  }

  if (!isTerminator(node.right)) {
    const red = node.level === node.right.level;
    const black = node.level - 1 === node.right.level;
    expect.true(red || black);

    if (!isTerminator(node.right.right)) {
      expect.true(node.right.right.level < node.level);
    }
    validateAaTreeConstraints(expect, node.right);
  }

  if (node.level > 1) {
    expect.false(isTerminator(node.left));
    expect.false(isTerminator(node.right));
  }
}
