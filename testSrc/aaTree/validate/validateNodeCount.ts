import { IExpect } from "@b08/test-runner";
import { treeCount } from "./treeCount";
import { AANode } from "../../../src/aaTree/aaNode.type";

export function validateNodeCount<TKey>(expect: IExpect, node: AANode<TKey>, count: number): void {
  expect.equal(treeCount(node), count);
}
