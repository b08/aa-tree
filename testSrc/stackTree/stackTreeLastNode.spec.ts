import { describe } from "@b08/test-runner";
import { StackTree } from "../../src";
import { createTestItem } from "../testType";

describe("stackTree", it => {
  it("shift should remove last item and return null", async expect => {
    // arrange
    let items = [4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    tree.shift();
    const result2 = tree.shift();

    // assert
    expect.true(result2 == null);
  });

  it("shiftGroup should remove last item", async expect => {
    // arrange
    let items = [4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    tree.shiftGroup();
    const result2 = tree.shiftGroup();

    // assert
    expect.true(result2 == null);
  });

  it("pop should remove last item", async expect => {
    // arrange
    let items = [4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    tree.pop();
    const result2 = tree.pop();

    // assert
    expect.true(result2 == null);
  });

  it("popGroup should remove last item", async expect => {
    // arrange
    let items = [4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    tree.popGroup();
    const result2 = tree.popGroup();

    // assert
    expect.true(result2 == null);
  });


});
