import { terminator } from "../../src/aaTree/terminator";
import { addStackItem } from "../../src/stackTree/addStackItem";
import { createUniqueItems } from "../testType";
import { validateTreeWeight } from "./validateTreeWeight";
import { validateNodeCount } from "../aaTree/validate/validateNodeCount";
import { validateAaTreeConstraints } from "../aaTree/validate/validateAaTreeConstraints";
import { validateTreeDepth } from "../aaTree/validate/validateTreeDepth";
import { describe } from "@b08/test-runner";

describe("addItem", it => {
  it("should add non unique items", async expect => {
    // arrange
    let items = createUniqueItems(500);
    items = [...items, ...items];
    let tree = terminator;

    // act
    items.forEach(item => tree = addStackItem(tree, item, item.key));

    // assert
    validateNodeCount(expect, tree, items.length / 2);
    validateTreeWeight(expect, tree, items.length);
    validateAaTreeConstraints(expect, tree);
    validateTreeDepth(expect, tree);
  });
});
