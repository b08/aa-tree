import { describe } from "@b08/test-runner";
import { StackTree } from "../../src";
import { createTestItem, TestType } from "../testType";

describe("stackTree", it => {
  it("should return first item", async expect => {
    // arrange
    let items = [4, 5, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.first();
    const result2 = tree.first();

    // assert
    expect.equal(result, items[2]);
    expect.equal(result2, items[2]);
  });

  it("first should return null off empty tree", async expect => {
    // arrange
    let tree = new StackTree((item: TestType) => item.key);

    // act
    const result = tree.first();

    // assert
    expect.true(result == null);
  });

  it("should return last item", async expect => {
    // arrange
    let items = [4, 5, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.last();
    const result2 = tree.last();

    // assert
    expect.equal(result, items[4]);
    expect.equal(result2, items[4]);
  });

  it("last should return null off empty tree", async expect => {
    // arrange
    let tree = new StackTree((item: TestType) => item.key);

    // act
    const result = tree.last();

    // assert
    expect.true(result == null);
  });


  it("should shift item", async expect => {
    // arrange
    let items = [4, 5, 2, 2, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.shift();
    const result2 = tree.shift();

    // assert
    expect.equal(result, items[2]);
    expect.equal(result2, items[3]);
  });

  it("should shift group of item", async expect => {
    // arrange
    let items = [4, 6, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.shiftGroup();

    // assert
    expect.deepEqual(result, [items[2]]);
  });

  it("shift should return null off empty tree", async expect => {
    // arrange
    let tree = new StackTree((item: TestType) => item.key);

    // act
    const result = tree.shift();
    const result2 = tree.shiftGroup();

    // assert
    expect.true(result == null);
    expect.true(result2 == null);
  });

  it("should pop item", async expect => {
    // arrange
    let items = [4, 6, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.pop();
    const result2 = tree.pop();

    // assert
    expect.equal(result, items[4]);
    expect.equal(result2, items[1]);
  });

  it("should pop group of item", async expect => {
    // arrange
    let items = [4, 6, 2, 3, 6, 4].map(createTestItem);

    let tree = new StackTree(item => item.key, ...items);

    // act
    const result = tree.popGroup();

    // assert
    expect.deepEqual(result, [items[1], items[4]]);
  });

  it("pop should return null off empty tree", async expect => {
    // arrange
    let tree = new StackTree((item: TestType) => item.key);

    // act
    const result = tree.pop();
    const result2 = tree.popGroup();

    // assert
    expect.true(result == null);
    expect.true(result2 == null);
  });

  it("should add item", async expect => {
    // arrange
    let items = [4, 5, 2].map(createTestItem);
    let tree = new StackTree(item => item.key, ...items);
    let newItem = createTestItem(1);

    // act
    tree.add(newItem, 1);
    const result = tree.first();

    // assert
    expect.equal(result, newItem);
  });

  it("isEmpty should work", async expect => {
    // arrange
    let tree = new StackTree(item => item.key, createTestItem(5));

    // act
    const empty1 = tree.isEmpty();
    tree.pop();
    const empty2 = tree.isEmpty();

    // assert
    expect.false(empty1);
    expect.true(empty2);
  });
});
