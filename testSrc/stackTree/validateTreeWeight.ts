import { IExpect } from "@b08/test-runner";
import { MultiValueNode } from "../../src/stackTree/multiValueNode.type";
import { isTerminator } from "../../src/aaTree/terminator";

export function validateTreeWeight<T, TKey>(expect: IExpect, node: MultiValueNode<T, TKey>, count: number): void {
  const weight = treeWeight(node);
  expect.equal(weight, count);
}

function treeWeight<T, TKey>(node: MultiValueNode<T, TKey>): number {
  return isTerminator(node) ? 0 : (node.items.length + treeWeight(node.left) + treeWeight(node.right));
}
